import QtQuick 2.11
import QtQuick.Window 2.11

Window {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("SPImage Test")
    property real scaleFactor: 1
    Text{
        anchors.centerIn: parent
        id:initText
        text:qsTr("Drop Image Here")
        font.pointSize: 20
        font.bold: true
    }

    Image {
        id: image
        focus: true
        width: image.sourceSize.width * root.scaleFactor
        height: image.sourceSize.height * root.scaleFactor
        anchors.centerIn: parent
        Keys.onPressed: {
            if (event.key === Qt.Key_Plus || event.key === Qt.Key_Equal) {
                root.scaleFactor += 0.2
            } else if (event.key === Qt.Key_Minus) {
                root.scaleFactor -= 0.2
            }
            event.accepted = true
        }
    }

    DropArea {
        id: imageDropArea
        anchors.fill: parent
        keys: "text/uri-list"
        onEntered: {
            if (drag.hasUrls === false) {
                drag.accept(Qt.IgnoreAction)
            }
        }
        onDropped: {
            if (drop.hasUrls === true) {
                image.source = drop.urls[0]
                initText.visible = false
                drop.accept()
            }
        }
    }
}
